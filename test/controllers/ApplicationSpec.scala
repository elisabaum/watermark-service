package controllers

import models.State.State
import models.Topic.Topic
import models._
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json._
import play.api.test.Helpers._
import play.api.test._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification {

  "Application" should {

    "return a ticket for a valid book" in new WithApplication {
      val request = Json.obj("type" -> "book", "document" -> Json.toJson(book))
      val response = route(FakeRequest(POST, "/createDocument").withJsonBody(request)).get
      status(response) mustEqual OK

      val content = contentAsJson(response)
      (content \ "id").as[Int] must not be null
      (content\ "state").as[State] mustEqual State.Initialized
    }

    "return a ticket for a valid journal" in new WithApplication {
      val request = Json.obj("type" -> "journal", "document" -> Json.toJson(journal))
      val response = route(FakeRequest(POST, "/createDocument").withJsonBody(request)).get
      status(response) mustEqual OK

      val content = contentAsJson(response)
      (content \ "id").as[Int] must not be null
      (content\ "state").as[State] mustEqual State.Initialized
    }

    "return no ticket for an invalid document" in new WithApplication {
      val request = Json.obj("documents" -> "invalid")
      val response = route(FakeRequest(POST, "/createDocument").withJsonBody(request)).get
      status(response) mustEqual BAD_REQUEST
      (contentAsJson(response) \ "message").as[String] must startWith("unknown json format")
    }

    "return no ticket for an invalid document type" in new WithApplication {
      val request = Json.obj("type" -> "magazine", "document" -> Json.toJson(book))
      val response = route(FakeRequest(POST, "/createDocument").withJsonBody(request)).get
      status(response) mustEqual BAD_REQUEST
      (contentAsJson(response) \ "message").as[String] must startWith("unknown document type")
    }

    "be able to check a ticket for a valid id" in new WithApplication {
      val ticketId = uploadDocumentAndGetTicket()
      val response = route(FakeRequest(GET, "/ticket/" + ticketId)).get
      status(response) mustEqual OK

      val json = contentAsJson(response)
      (json \ "id").as[Int] mustEqual ticketId
      (json \ "state").as[State] mustEqual State.Running
    }

    "return a bad request for an invalid id" in new WithApplication {
      val response = route(FakeRequest(GET, "/ticket/42")).get
      status(response) mustEqual BAD_REQUEST
      contentAsString(response) must startWith("could not find ticket")
    }

    "return a document with watermark for a valid ticket id" in new WithApplication {
      val ticketId = uploadDocumentAndGetTicket()

      Thread sleep (1000 * 6)

      val response = route(FakeRequest(GET, "/document/" + ticketId)).get
      status(response) mustEqual OK
      val content = contentAsJson(response)
      val watermark = content \ "watermark"
      watermark must not be null
      (watermark \ "content").as[String] mustEqual "book"
      (watermark \ "title").as[String] mustEqual "The Scala Programming Language"
      (watermark \ "author").as[String] mustEqual "Martin Odersky"
      (watermark \ "topic").as[Topic] mustEqual Topic.Science
    }

    "return a bad request for a document for which watermark is not generated yet" in new WithApplication {
      val ticketId = uploadDocumentAndGetTicket()

      Thread sleep (1000 * 3)

      val response = route(FakeRequest(GET, "/document/" + ticketId)).get
      status(response) mustEqual BAD_REQUEST
      contentAsString(response) must startWith(s"ticket with id '$ticketId' is not finished yet")
    }

    "return a bad request for a document with an invalid id" in new WithApplication {
      val response = route(FakeRequest(GET, "/document/42")).get
      status(response) mustEqual BAD_REQUEST
      contentAsString(response) must startWith("could not find ticket")
    }

    "send 404 on a bad request" in new WithApplication{
      route(FakeRequest(GET, "/boum")) must beNone
    }

  }

  private def book = Book("The Scala Programming Language", "Martin Odersky", Topic.Science)

  private def journal = Journal("Interesting Scala", "Elisa Baum")

  private def uploadDocumentAndGetTicket(): Int = {
    val request = Json.obj("type" -> "book", "document" -> Json.toJson(book))
    val response = route(FakeRequest(POST, "/createDocument").withJsonBody(request)).get
    val content = contentAsJson(response)
    (content \ "id").as[Int]
  }

}
