package services

import org.specs2.mutable.Specification

/**
 * Created by Elisa Baum (adesso AG) on 13.07.15.
 */
class NotificationServiceSpec extends Specification with EventSource[String] {

    "A NotificationService" should {

      "inform its observers about changing state" in {
        var changed = false
        add(new Event[String] {
          def event(ev: String) {
            changed = true
          }
        })
        notify("Changed")
        changed mustEqual true
      }
      "should not inform observer when added and then got removed" in {
        var changed = false
        val listener = new Event[String] {
          def event(ev: String) {
            changed = true
          }
        }
        remove(add(listener))
        notify("Changed")
        changed mustEqual false
      }

    }
}
