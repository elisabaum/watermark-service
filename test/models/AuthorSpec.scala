package models

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.libs.json.Json

/**
 * Created by Elisa Baum (adesso AG) on 08.07.15.
 */

@RunWith(classOf[JUnitRunner])
class AuthorSpec extends Specification {

  "Creating an Author" should {
    "throw an IllegalArgumentException for firstName = null" in {
      Author(null, "Mustermann") must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException for lastName = null" in {
      Author("Max", null) must throwA[IllegalArgumentException]
    }

    "return a valid Author for firstName and lastName not null" in {
      val author = Author("Max", "Mustermann")
      author must not be null
      author.firstName mustEqual "Max"
      author.lastName mustEqual "Mustermann"
    }

    "return a valid author for a string (implicit)" in {
      ("Max Mustermann": Author) mustEqual Author("Max", "Mustermann")
    }

    "throw an IllegalArgumentException for an invalid string (implicit)" in {
      ("Mustermann": Author) must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException for null (implicit)" in {
      Author.fromString(null) must throwA[IllegalArgumentException]
    }
  }

  "An Author" should {
    "be serializable to JSON" in {
      val author = Author("Max", "Mustermann")
      val json = Json.toJson(author)
      (json \ "firstName").as[String] mustEqual "Max"
      (json \ "lastName").as[String] mustEqual "Mustermann"
    }

    "be deserializable from JSON" in {
      val author = Author("Max", "Mustermann")
      val json = Json.toJson(author)
      Json.fromJson[Author](json).get mustEqual author
    }
  }

}
