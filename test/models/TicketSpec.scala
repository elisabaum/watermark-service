package models

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.libs.json.Json
import models.State.State

/**
 * Created by Elisa Baum (adesso AG) on 09.07.15.
 */
@RunWith(classOf[JUnitRunner])
class TicketSpec extends Specification {

  "Creating a Ticket" should {
    "throw an IllegalArgumentException with document = null" in {
      Ticket(null) must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException with a negative id" in {
      Ticket(document, -1) must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException with progress smaller than 0 or greater than 1" in {
      Ticket(document, progress = -1.0) must throwA[IllegalArgumentException]
      Ticket(document, progress = 2.0) must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException with state = null" in {
      Ticket(document, state = null) must throwA[IllegalArgumentException]
     }

    "return a valid ticket for valid parameters" in {
      val t = ticket
      t mustNotEqual null
    }
  }

  "A Ticket" should {
    "be in state Initialized on creation" in {
      ticket.state mustEqual State.Initialized
    }

    "be able to change its state from Initialized to Running" in {
      val t = ticket
      val startedTicket: Ticket = t.start
      startedTicket.state mustEqual State.Running
    }

    "be able to change its state from Running to Finished" in {
      val t = ticket
      val startedTicket = t.start
      val finishedTicket = startedTicket.finish
      finishedTicket.state mustEqual State.Finished
    }

    "not be able to change its state from Finished to Running" in {
      val t = ticket
      val startedTicket = t.start
      val finishedTicket = startedTicket.finish
      finishedTicket.start must throwA[IllegalStateException]
    }

    "not be able to change its state from Initialized to Finished" in {
      val t = ticket
      t.finish must throwA[IllegalStateException]
    }

    "return a copy of itself while changing its state" in {
      val t = ticket
      val startedTicket: Ticket = t.start
      t mustNotEqual startedTicket
    }

    "be serializable to JSON" in {
      val t = ticket
      val json = Json.toJson(t)
      (json \ "id").as[Int] mustEqual t.id
      (json \ "state").as[State] mustEqual t.state
      (json \ "progress").as[Double] mustEqual t.progress
    }
  }

  private def document = Book("Play For Scala", "Peter Hilton", Topic.Science)

  private def ticket = Ticket(document)
}
