package models

import models.Topic.Topic
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import play.api.libs.json.Json

/**
 * Created by Elisa Baum (adesso AG) on 09.07.15.
 */
@RunWith(classOf[JUnitRunner])
class DocumentSpec extends Specification {

  "Creating a Document" should {
    "throw an IllegalArgumentException if title = null" in {
      Journal(null, "Max Mustermann") must throwA[IllegalArgumentException]
    }

    "throw an IllegalArgumentException if author = null" in {
      Journal("Play For Scala", null) must throwA[IllegalArgumentException]
    }
  }

  "A Book" should {
    "throw an IllegalArgumentException if topic = null" in {
      Book("Play For Scala", "Peter Hilton", null) must throwA[IllegalArgumentException]
    }

    "be serializable to JSON" in {
      val book = Book("Play For Scala", "Peter Hilton", Topic.Science)
      val json = Json.toJson(book)
      (json \ "title").as[String] mustEqual "Play For Scala"
      (json \ "author").as[Author] mustEqual Author("Peter", "Hilton")
      (json \ "topic").as[Topic] mustEqual Topic.Science
    }

    "be deserializable from JSON" in {
      val book = Book("Play For Scala", "Peter Hilton", Topic.Science)
      val json = Json.toJson(book)
      Json.fromJson[Book](json).get mustEqual book
    }
  }

  "A Journal" should {
    "be serializable to JSON" in {
      val journal = Journal("Interesting Scala", "Elisa Baum")
      val json = Json.toJson(journal)
      (json \ "title").as[String] mustEqual "Interesting Scala"
      (json \ "author").as[Author] mustEqual Author("Elisa", "Baum")
    }

    "be deserializable from JSON" in {
      val journal = Journal("Interesting Scala", "Elisa Baum")
      val json = Json.toJson(journal)
      Json.fromJson[Journal](json).get mustEqual journal
    }
  }
}
