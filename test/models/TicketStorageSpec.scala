package models

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

/**
 * Created by Elisa Baum (adesso AG) on 09.07.15.
 */
@RunWith(classOf[JUnitRunner])
class TicketStorageSpec extends Specification {

  "The TicketStorage" should {
    "return nothing when no ticket for a given id exists" in {
      TicketStorage.get(42) must beNone
    }

    "be able to update a ticket and store and return the changed ticket" in {
      val t = ticket
      TicketStorage.put(t)
      val updatedTicket = TicketStorage.update(t, t => t.start)
      updatedTicket.id mustEqual t.id
      updatedTicket.state mustEqual State.Running
      updatedTicket must be equalTo TicketStorage.get(updatedTicket.id).get
    }

    "be able to delete a stored ticket" in {
      val t = ticket
      TicketStorage.put(t)
      TicketStorage.get(t.id) must beSome[Ticket]
      TicketStorage.delete(t.id)
      TicketStorage.get(t.id) must beNone
    }

    "throw an IllegalArgumentException on update for unstored tickets" in {
      val t = ticket
      TicketStorage.update(t, t => t.start) must throwA[IllegalArgumentException]
    }

  }

  def ticket = Ticket(Journal("Interesting Scala", "Elisa Baum"))

}
