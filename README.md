Springer Watermark Test 
------------------
This project realizes an asynchronous watermarking service for documents (books or journals). The service is implemented as scala play web service, supporting two client implementations using HTTP polling and websockets communication.

Run
------------------
The server application can be hit with 

	> sbt run

Client has fixed resource connection to http://127.0.0.1:9000.

Test
------------------
The test feature can be hit with 

    > sbt test

Requires
------------------
* SBT 		        0.13.5
* Scala 		2.11.1
* Specs 2 		2.3.12
* sbt-scoverage 	1.1.0
* PlayFramework         2.3.8
* jQuery
* Twitter Bootstrap

Author
--------------------
Elisa Baum