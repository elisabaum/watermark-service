/**
 * Created by baum on 12.07.15.
 */
(function() {

    var webSocketURL = 'ws://localhost:9000/websocket';
    var httpCreateDocumentURL = 'http://localhost:9000/createDocument';
    var httpCheckTicketURL = 'http://localhost:9000/ticket/';

    var webSocket;
    var sendDataHandler = [];

    function generateJSONForDocument() {
        return JSON.stringify({type: $('input[name=documentType]:checked').val(), document: {title: $("#title").val(), author: {firstName: $("#firstName").val(), lastName: $("#lastName").val()}, topic: $("#topic").val()}});
    }

    function logMessage(message) {
        $("#log").append(message + "\n");
    }

    function checkTicket(ticketId) {
        $.ajax({
            url: httpCheckTicketURL + ticketId,
            type: 'GET',
            crossDomain: 'true',
            success: function(data) {
                var state = data.state;
                if (state == 'Running') {
                    watermarkingProgress(data.progress * 100);
                    setTimeout(function() {checkTicket(ticketId)}, 1000);
                }
                else if (state == 'Finished') {
                    watermarkingFinished();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                logMessage('Error: ' + errorThrown);
            }
        });
    }

    function toggleTopic() {
        var $topic = $('#topicContainer');
        var documentType = $('input[name=documentType]:checked').val();

        if(documentType == 'journal') {
            $topic.hide();
        }
        else {
            $topic.show();
        }
    }

    function httpSubmit() {
        $.ajax({
            url: httpCreateDocumentURL,
            type: 'POST',
            data: generateJSONForDocument(),
            dataType: 'json',
            contentType:'application/json; charset=utf-8',
            crossDomain: true,
            success: function(data) {
                logMessage('Ticket for document via HTTP received');
                checkTicket(data.id);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                logMessage('Error: ' + errorThrown);
            }
        });
    }

    function watermarkingProgress(progress) {
        $('#progressbar')
            .removeClass('progress-bar-success')
            .addClass('progress-bar-striped active');
        $('.progress-bar').css('width', progress + '%').attr('aria-valuenow', progress);
    }

    function watermarkingFinished() {
        $('#progressbar')
            .removeClass('progress-bar-striped active')
            .addClass('progress-bar-success');
        logMessage('Watermarking finished')
    }

    function sendDataViaWebSocket() {
        if (!webSocket) {
            createWebSocket();
        }

        if (webSocket.readyState == WebSocket.OPEN) {
            send();
        }
        else {
            sendDataHandler.push(send);
        }

        function send() {
            webSocket.send(generateJSONForDocument());
        }
    }

    function createWebSocket() {
        webSocket = new WebSocket(webSocketURL);
        webSocket.onmessage = function(event) {
            var jsonMessage = jQuery.parseJSON(event.data);
            var state = jsonMessage.state;

            if (state == 'Running') {
                watermarkingProgress(jsonMessage.progress * 100);
            }
            else if(state == 'Finished') {
                watermarkingFinished();
            }
        };

        webSocket.onopen = function() {
            logMessage("WebSocket connected");
            sendDataHandler.forEach(function (handler) {
                handler();
            })
        };

        webSocket.onclose = function() {
            logMessage("WebSocket disconnected");
            webSocket = null;
        };
    }

    $(document).ready(function() {
        $('#journal').change(toggleTopic);

        $('#book').change(toggleTopic);

        $('#documentForm').submit(function(e) {
            e.preventDefault();

            var protocol = $('input[name=protocol]:checked').val();

            if (protocol == 'websockets') {
                sendDataViaWebSocket();
            }
            else {
                httpSubmit();
            }
        });
    });
}());

