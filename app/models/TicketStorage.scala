package models

/**
 * Created by Elisa Baum (adesso AG) on 09.07.15.
 */
object TicketStorage {

  private var storage = List[Ticket]()

  def put(ticket: Ticket): Unit = storage = storage.::(ticket)

  def get(key: Int): Option[Ticket] = storage.find(_.id == key)

  def update(oldTicket: Ticket, f: Ticket => Ticket): Ticket = {
    val index = storage.indexOf(oldTicket)

    if (index == -1) {
      throw new IllegalArgumentException(s"ticket '$oldTicket' could not be found in ticket storage")
    }
    else {
      val newTicket: Ticket = f(oldTicket)
      storage = storage.updated(index, newTicket)
      newTicket
    }
  }

  def delete(key: Int) = {
    storage = storage.dropWhile(t => t.id == key)
  }
}
