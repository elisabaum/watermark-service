package models

import models.State.State
import play.api.libs.json._
import utils.EnumUtil
import scala.util.Random

/**
 * Created by Elisa Baum (adesso AG) on 08.07.15.
 */
case class Ticket(document: Document, id: Int = Random.nextInt(Int.MaxValue), var progress: Double = 0.0, state: State = State.Initialized) {
  require(document != null, "document must be not null")
  require(id >= 0, "key must be >= 0")
  require(progress >= 0.0 && progress <= 1.0, "progress must be between [0.0, 1.0]")
  require(state != null, "state must be not null")

  def start: Ticket = {
    if (state == State.Initialized)
      copy(state = State.Running)
    else
      throw new IllegalStateException(s"Can't start document, state was '$state' instead of Initialized!")
  }

  def doProgress(currentProgress: Double): Ticket = {
    if (state == State.Running)
      copy(progress = currentProgress)
    else
      throw new IllegalStateException(s"Can't do progress for document, state was '$state' instead of Running!")
  }

  def finish: Ticket = {
    if (state == State.Running)
      copy(state = State.Finished, progress = 1.0)
    else
      throw new IllegalStateException(s"Can't finish document, state was '$state' instead of Running!")
  }
}

object Ticket {

  implicit val ticketWrites: Writes[Ticket] = new Writes[Ticket] {
    override def writes(ticket: Ticket): JsValue = {
      Json.obj("id" -> ticket.id, "state" -> ticket.state, "progress" -> ticket.progress)
    }
  }
}

object State extends Enumeration {
  type State = Value
  val Initialized, Running, Finished = Value

  implicit val enumReads: Reads[State] = EnumUtil.enumReads(State)
  implicit val enumWrites: Writes[State] = EnumUtil.enumWrites
}
