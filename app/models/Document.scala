package models

import models.Topic.Topic
import utils.EnumUtil
import play.api.libs.json._
import play.api.libs.functional.syntax._


/**
 * Created by Elisa Baum (adesso AG) on 08.07.15.
 */

trait Watermarkable {
  type Watermark = Map[String, String]
  protected var watermark: Option[Watermark] = None

  def isWatermarked: Boolean = watermark.isDefined
  def changeWatermark(w: Watermark): Watermark = {
    watermark match {
      case None => watermark = Some(w)
        watermark.get
      case _ =>  watermark.get
    }
  }
}

sealed abstract class Document(val title: String, val author: Author) extends Watermarkable {
  require(title != null, "title must be not null")
  require(author != null, "author must be not noll")

  def generateWatermark: Watermark

  def toJson: JsObject = {
    val json: JsObject = generateJson.as[JsObject]
    json.++(Json.obj("watermark" -> watermark))
  }

  protected def generateJson: JsValue
}

case class Book(bookTitle: String, bookAuthor: Author, topic: Topic) extends Document(bookTitle, bookAuthor) {
  require(topic != null, "topic must be not null")

  override def generateWatermark: Watermark = {
    changeWatermark(Map(("content", "book"), ("title", bookTitle), ("author", bookAuthor.toString), ("topic", topic.toString)))
  }

  override protected def generateJson: JsValue = Json toJson this
}

object Book {
  implicit val bookWrites: Writes[Book] = (
      (JsPath \ "title").write[String] and
      (JsPath \ "author").write[Author] and
      (JsPath \ "topic").write[Topic]
    )(unlift(Book.unapply))

  implicit val bookReads: Reads[Book] = (
      (JsPath \ "title").read[String] and
      (JsPath \ "author").read[Author] and
      (JsPath \ "topic").read[Topic]
    )(Book.apply _)
}

case class Journal(journalTitle: String, journalAuthor: Author) extends Document(journalTitle, journalAuthor) {
  override def generateWatermark: Watermark = {
    changeWatermark(Map(("content", "journal"), ("title", journalTitle), ("author", journalAuthor.toString)))
  }

  override protected def generateJson: JsValue = Json toJson this
}

object Journal {
  implicit val journalWrites: Writes[Journal] = (
      (JsPath \ "title").write[String] and
      (JsPath \ "author").write[Author]
    )(unlift(Journal.unapply))

  implicit val journalReads: Reads[Journal] = (
      (JsPath \ "title").read[String] and
      (JsPath \ "author").read[Author]
    )(Journal.apply _)
}

object Topic extends Enumeration {
  type Topic = Value
  val Business, Science, Media = Value

  implicit val enumReads: Reads[Topic] = EnumUtil.enumReads(Topic)
  implicit val enumWrites: Writes[Topic] = EnumUtil.enumWrites
}

