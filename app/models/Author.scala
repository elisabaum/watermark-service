package models

import play.api.libs.json._

/**
 * Created by Elisa Baum (adesso AG) on 08.07.15.
 */

case class Author(firstName: String, lastName: String) {
  require(firstName != null, "firstName must be not null")
  require(lastName != null , "lastName must be not null")

  override val toString = s"$firstName $lastName"
}

object Author {
  implicit val authorWrites = Json.writes[Author]
  implicit val authorReads = Json.reads[Author]

  implicit def fromString(s: String) : Author = {
    require(s != null, "author must not be null")

    val tokens = s.split(" ")
    if (tokens.length == 2)
      Author(tokens(0), tokens(1))
    else
      throw new IllegalArgumentException(s"Cannot convert String $s to author!")
  }
}
