package utils

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

/**
 * Created by Elisa Baum (adesso AG) on 12.07.15.
 */
class CorsFilter extends EssentialFilter {
  def apply(next: EssentialAction) = new EssentialAction {
    def apply(requestHeader: RequestHeader) = {
      next(requestHeader).map { result =>
        result.withHeaders(
          "Access-Control-Allow-Origin" -> "*",
          "Access-Control-Allow-Methods" -> "HEAD, POST, GET, OPTIONS, PUT, DELETE",
          "Access-Control-Allow-Headers" -> "Origin, X-Requested-With, Content-Type, Accept, User-Agent")
      }
    }
  }
}
