import play.api.GlobalSettings
import play.api.mvc.WithFilters
import utils.CorsFilter

/**
 * Created by Elisa Baum (adesso AG) on 12.07.15.
 */
object Global extends WithFilters(new CorsFilter) with GlobalSettings
