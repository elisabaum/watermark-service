package services

import scala.util.Random

/**
 * Created by Elisa Baum (adesso AG) on 10.07.15.
 */

trait Event[A] {
  def event(ev: A)
}

trait EventSource[A] {
  var listeners: Map[Long, Event[A]] = Map()

  def notify(ev: A) = for (l <- listeners.values) l.event(ev)

  def add(listener: Event[A]) : Long = {
    val key: Long = Random.nextInt(Integer.MAX_VALUE)
    listeners = listeners + (key -> listener)
    key
  }

  def remove(key: Long) = listeners = listeners - key
}
