package services

import models.{TicketStorage, Ticket}
import play.api.libs.json.{Json, JsValue}

import scala.concurrent.Future


/**
 * Created by Elisa Baum (adesso AG) on 08.07.15.
 */
object WatermarkService extends EventSource[JsValue] {

  import play.api.libs.concurrent.Execution.Implicits.defaultContext

  def generateWatermark(ticket: Ticket): Future[Ticket] = Future[Ticket] {
    TicketStorage.put(ticket)
    notify(Json.toJson(ticket))

    var currentTicket = TicketStorage.update(ticket, t => t.start)

    // simulate some work to show async mode
    for (x <- 1 to 5) {
      currentTicket = TicketStorage.update(currentTicket, t => t.doProgress(0.2 * x))
      notify(Json.toJson(currentTicket))
      Thread sleep 1000
    }

    currentTicket.document.generateWatermark

    val finishedTicket: Ticket = TicketStorage.update(currentTicket, t => t.finish)
    notify(Json.toJson(finishedTicket))
    finishedTicket
  }

}
