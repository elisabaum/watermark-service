package controllers

import models._
import play.api.libs.iteratee.{Concurrent, Iteratee}
import play.api.libs.json._
import play.api.mvc._
import services.{Event, WatermarkService}
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Play.current

object Application extends Controller {

  def documents = Action {
    Ok(views.html.documents())
  }

  /**
   * Creates a new ticket for a document(book or journal) posted as json and starts watermarking service.
   * Returns a BadRequest im json do not match the specification.
   * @param request.body - the document encoded in json
   *                       Example: {"type": "book", "document": {"title": "The Scala Programming Language","author": {"firstName": "Martin", "lastName": "Odersky"}, "topic": "Science"}}
   *                       The property 'type' defines whether it is a book or a journal.
   *
   * @return the ticket or error message as json (the property 'state' indicates the success with 'ok' and 'error')
   */
  def createDocument = Action(parse.json) {request =>
    generateWatermark(request.body.as[JsObject], jsValue => Ok(jsValue), jsValue => BadRequest(jsValue))
  }

  /**
   * Checks the state of the ticket with the given id.
   * State could be 'Initialized', 'Running' or 'Finished' depending on the watermarking service.
   * Returns a BadRequest if ticket could not be found.
   *
   * @param ticketId - the ticketId of the requested ticket
   * @return the ticket with its actual state as json or a error message
   */
  def checkTicket(ticketId: Int) = Action {
    invokeTicketAndExecute(ticketId, t => Ok(Json.toJson(t)))
  }

  /**
   * Returns a document for a ticket with the given id if available, meaning if watermarking service finished the document.
   * Otherwise returning a BadRequest.
   *
   * @param ticketId - the ticketId of the ticket which is associated with the document
   * @return the document as json or error message
   */
  def getDocument(ticketId: Int) = Action {
    val getDocumentIfFinished = (ticket: Ticket) => {
      if (ticket.state == State.Finished)
        Ok(ticket.document.toJson)
      else
        BadRequest(s"ticket with id '$ticketId' is not finished yet (progress: ${ticket.progress * 100}%), try again later")
    }

    invokeTicketAndExecute(ticketId, getDocumentIfFinished)
  }

  def options(url: String) = Action {
    NoContent.withHeaders(
      "Access-Control-Allow-Origin" -> "*",
      "Access-Control-Allow-Methods" -> "POST, GET, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers" -> "Origin, X-Requested-With, Content-Type, Accept, User-Agent")
  }

  /**
   * Open a bidirectional connection after sending a document as Json and pushes updates about the state of watermarking.
   * WebSocket Resource <ws://localhost:9000/websocket>
   */
  def websocket = WebSocket.using[JsValue] { request =>
    val (out, channel) = Concurrent.broadcast[JsValue]
    WatermarkService.add(new Event[JsValue] {
      def event(ev: JsValue) {
        channel.push(ev)
      }
    })

    val in = Iteratee.foreach[JsValue] { message => {
        generateWatermark(message.as[JsObject], jsValue => channel push jsValue, jsValue => channel push jsValue)
      }
    }

    (in, out)
  }

  private def generateWatermark[R](message: JsObject, out: JsValue => R, error: JsValue => R): R = {
    val jsDocumentType = message.fields.find(_._1 == "type")

    jsDocumentType match {
      case Some(tuple) =>
        tuple._2.as[JsString].value match {
          case "journal" => doGenerateWatermark[R, Journal]((message\"document").validate[Journal], out, error)
          case "book" => doGenerateWatermark[R, Book]((message\"document").validate[Book], out, error)
          case _ => error(Json.obj("state" -> "error", "message" -> s"unknown document type '${tuple._2.toString()}'"))
        }
      case _ => error(Json.obj("state" -> "error", "message" ->"unknown json format"))
    }
  }

  private def doGenerateWatermark[R, T <: Document](jsDocument: JsResult[T], out: JsValue => R, error: JsValue => R): R = {
    jsDocument match {
      case success: JsSuccess[T] =>
        val ticket = Ticket(success.get)
        WatermarkService.generateWatermark(ticket)
        out(Json.toJson(ticket))
      case e: JsError =>
        error(Json.obj("state" -> "error", "message" -> JsError.toFlatJson(e)))
    }
  }

  private def invokeTicketAndExecute(ticketId: Int, f: Ticket => Result): Result = {
    val ticket = TicketStorage.get(ticketId)

    ticket match {
      case Some(t:Ticket) => f(t)
      case _ => BadRequest(s"could not find ticket with id '$ticketId'")
    }
  }
}